package com.datnst.smartschool.adminapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datnst.smartschool.adminapi.entities.Actor;
import com.datnst.smartschool.adminapi.repositories.ActorRepository;

@Service
public class ActorService {
	private final ActorRepository actorRepository;
	@Autowired
	public ActorService(ActorRepository actorRepository) {
		super();
		this.actorRepository = actorRepository;
	}
	
	public List<Actor> getAllActors () {
		List<Actor> actors = new ArrayList<>();
		
		actorRepository.findAll().forEach(actor -> {
			Actor migrateActor = new Actor();
			migrateActor.setActorId(actor.getActorId());
			migrateActor.setFirstName(actor.getFirstName());
			migrateActor.setLastName(actor.getLastName());
			migrateActor.setLastUpdate(actor.getLastUpdate());
			actors.add(migrateActor);
		});
		
		return actors;
	}
	
	
	
}
