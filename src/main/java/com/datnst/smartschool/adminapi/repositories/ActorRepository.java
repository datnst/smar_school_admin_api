package com.datnst.smartschool.adminapi.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import com.datnst.smartschool.adminapi.entities.Actor;

@Repository
public interface ActorRepository  extends CrudRepository<Actor, Long>{

}
