package com.datnst.smartschool.adminapi.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datnst.smartschool.adminapi.entities.Actor;
import com.datnst.smartschool.adminapi.services.ActorService;


@RestController
@RequestMapping("/api")
public class ActorRestController {
		
	private ActorService actorService;

	public ActorRestController(ActorService actorService) {
		this.actorService = actorService;
	}

	@GetMapping("/actors")
	public List<Actor> getAllActors() {
		return this.actorService.getAllActors();
	}
}
